FROM devops-registry.ekenya.co.ke/mobile-banking/mb-eureka-server-builder:latest as builder
WORKDIR /app
COPY . .
RUN mvn package -Dmaven.test.skip=true

FROM eclipse-temurin:11-jre-alpine
WORKDIR /app
COPY  --from=builder /app/target/*.jar /app/app.jar
ENTRYPOINT ["java","-jar","app.jar"]
